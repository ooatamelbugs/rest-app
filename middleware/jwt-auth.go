package middleware

import (
	"log"
	"net/http"
	"rest-app/helper"
	"rest-app/service"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func AuthorizdJWT(jwtservice service.JWTService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			response := helper.ResponseReturned(false, "filad to un Authorizd", nil, "no token")
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		token, err := jwtservice.ValidateToken(authHeader)
		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			log.Println("claims[user_id]: ", claims["user_id"])
			log.Println("claims[issuer]: ", claims["issuer"])
		} else {
			log.Println(err)
			response := helper.ResponseReturned(false, "expire token", nil, err.Error())
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		}
	}
}
