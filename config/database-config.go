package config

import (
	"fmt"
	"os"
	"rest-app/entity"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// set up connection of db and creating new
func ConnectionDB() *gorm.DB {
	err := godotenv.Load() // load env

	// check if err is not empty
	if err != nil {
		panic("faild to load env")
	}

	// load db variables
	dbUser := os.Getenv("DB_USER")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")
	dbPass := os.Getenv("DB_PASSWORD")

	// make string connection to db
	dns := fmt.Sprint("host=", dbHost, " user=", dbUser, " password=", dbPass, " dbname=", dbName, " port=", dbPort, "?charset=utf8&parseTime=True")
	fmt.Println(dns)
	// connect to db
	db, err := gorm.Open(postgres.Open(dns), &gorm.Config{})

	// check if err is not empty
	if err != nil {
		panic("faild to connect to db")
	}
	db.AutoMigrate(&entity.Admin{}, &entity.Category{}, &entity.Customer{}, &entity.Meal{}, &entity.Order{}, &entity.OrderDetails{}, &entity.User{}, &entity.Cart{}, &entity.Coupon{}, &entity.FavoriteList{}, &entity.CartDetails{})
	return db
}

// close the connection to db
func CloseConnectionDB(dbConnection *gorm.DB) {
	db, err := dbConnection.DB()
	// check if err is not empty
	if err != nil {
		panic("falid to close db")
	}
	// close the connection
	db.Close()
}
