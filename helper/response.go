package helper

import "strings"

type Response struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Errors  interface{} `json:"errors"`
	Data    interface{} `json:"data"`
}

// respose data returned from request
func ResponseReturned(status bool, message string, data interface{}, err string) Response {
	var errorMessages interface{}
	if err != "" {
		errorMessages = strings.Split(err, "\n")
	}
	res := Response{
		Status:  status,
		Message: message,
		Errors:  errorMessages,
		Data:    data,
	}
	return res
}
