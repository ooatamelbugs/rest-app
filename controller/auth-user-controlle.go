package controller

import (
	"net/http"
	"rest-app/dto"
	"rest-app/entity"
	"rest-app/helper"
	"rest-app/service"
	"strconv"

	"github.com/gin-gonic/gin"
)

type AuthUserController interface {
	Login(ctx *gin.Context)
	Regester(ctx *gin.Context)
}

type authUserController struct {
	authService service.AuthUserService
	jwtService  service.JWTService
}

func NewAuthUserController(authUserDataController service.AuthUserService, jwtService service.JWTService) AuthUserController {
	return &authUserController{
		authService: authUserDataController,
		jwtService:  jwtService,
	}
}

func (c *authUserController) Login(ctx *gin.Context) {
	var loginDTO dto.LoginDTO
	errDTO := ctx.ShouldBind(&loginDTO)
	if errDTO != nil {
		response := helper.ResponseReturned(false, "error input", nil, errDTO.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	authResult := c.authService.VerifyCredetial(loginDTO.UserName, loginDTO.Password)
	if v, ok := authResult.(entity.User); ok {
		generateToken := c.jwtService.GenrateToken(strconv.FormatUint(v.ID, 10), v.UserName)
		v.Token = generateToken
		c.authService.UpdateUserToken(v.ID, generateToken)
		response := helper.ResponseReturned(true, "get your token", v, "")
		ctx.JSON(http.StatusAccepted, response)
		return
	}
	response := helper.ResponseReturned(false, " invalide Credetial", nil, "please check the correct username or password")
	ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
}

func (c *authUserController) Regester(ctx *gin.Context) {
	var registerDTO dto.UserCreateDTO
	errDTO := ctx.ShouldBind(&registerDTO)
	if errDTO != nil {
		response := helper.ResponseReturned(false, "error input", nil, errDTO.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	if !c.authService.IsDuplicateUsername(registerDTO.UserName) {
		response := helper.ResponseReturned(false, "error input", nil, "this username is already exist")
		ctx.JSON(http.StatusConflict, response)
	} else {
		createUser := c.authService.CreateUser(registerDTO)
		token := c.jwtService.GenrateToken(strconv.FormatUint(createUser.ID, 10), createUser.UserName)
		createUser.Token = token
		c.authService.UpdateUserToken(createUser.ID, token)
		response := helper.ResponseReturned(true, "get your token", createUser, "")
		ctx.JSON(http.StatusAccepted, response)
	}
}
