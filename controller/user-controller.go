package controller

import "github.com/gin-gonic/gin"

type UserController interface {
	UpdateUser(ctx *gin.Context)
	AddToCart(ctx *gin.Context)
	RemoveFromCart(ctx *gin.Context)
	AddToFavoriteList(ctx *gin.Context)
	RemoveFromFavoriteList(ctx *gin.Context)
	CreateOrder(ctx *gin.Context)
}
