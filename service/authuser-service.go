package service

import (
	"log"
	"rest-app/dto"
	"rest-app/entity"
	"rest-app/repository"

	"github.com/mashingan/smapping"
	"golang.org/x/crypto/bcrypt"
)

type AuthUserService interface {
	IsDuplicateUsername(UserName string) bool
	VerifyCredetial(UserName string, Password string) interface{}
	FindByUsername(UserName string) entity.User
	UpdateUserToken(userID uint64, token string) entity.User
	CreateUser(user dto.UserCreateDTO) entity.User
}

type authService struct {
	userRepository repository.UserRepository
}

func NewAuthUserService(userRepo repository.UserRepository) AuthUserService {
	return &authService{
		userRepository: userRepo,
	}
}

func (service *authService) VerifyCredetial(UserName string, Password string) interface{} {
	res := service.userRepository.VerifyCredetial(UserName, Password)
	if v, ok := res.(entity.User); ok {
		comparePassword := comparePassword(v.Password, []byte(Password))
		if v.UserName == UserName && comparePassword {
			return res
		}
		return false
	}
	return false
}

func (service *authService) UpdateUserToken(userID uint64, token string) entity.User {
	res := service.userRepository.UpdateUserToken(userID, token)
	return res
}

func comparePassword(hashedPassword string, plainPassword []byte) bool {
	byteHashed := []byte(hashedPassword)
	err := bcrypt.CompareHashAndPassword(byteHashed, plainPassword)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func (service *authService) CreateUser(user dto.UserCreateDTO) entity.User {
	newUser := entity.User{}
	err := smapping.FillStruct(&newUser, smapping.MapFields(&user))
	if err != nil {
		log.Fatalf("falid to map %v", err)
	}
	res := service.userRepository.InsertUser(newUser)
	return res
}

func (service *authService) FindByUsername(UserName string) entity.User {
	return service.userRepository.FindByUsername(UserName)
}

func (service *authService) IsDuplicateUsername(UserName string) bool {
	res := service.userRepository.IsDuplicateUsername(UserName)
	return !(res.Error == nil) // not(not error)
}
