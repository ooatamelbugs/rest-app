package service

import (
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type JWTService interface {
	GenrateToken(userID string, userName string) string
	ValidateToken(token string) (*jwt.Token, error)
}

type jwtUserClaim struct {
	UserID   string `json:"user_id"`
	UserName string `json:"username"`
	jwt.StandardClaims
}

type jwtService struct {
	secretkey string
	issuer    string
}

func NewJWTService(typeJWT string) JWTService {
	return &jwtService{
		issuer:    "ydhrub",
		secretkey: getSecretkey(typeJWT),
	}
}

func getSecretkey(jwtType string) string {
	var secretKey string
	switch jwtType {
	case "user":
		secretKey = os.Getenv("JWT_USER")
	case "customer":
		secretKey = os.Getenv("JWT_CUST")
	case "admin":
		secretKey = os.Getenv("JWT_ADMIN")
	}
	if secretKey != "" {
		secretKey = "ydhrwb"
	}
	return secretKey
}

func (j *jwtService) GenrateToken(UserID string, UserName string) string {
	claims := &jwtUserClaim{
		UserID,
		UserName,
		jwt.StandardClaims{
			ExpiresAt: time.Now().AddDate(1, 0, 0).Unix(),
			Issuer:    j.issuer,
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(j.secretkey))

	if err != nil {
		panic(err)
	}
	return t
}

func (j *jwtService) ValidateToken(token string) (*jwt.Token, error) {
	return jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected singing method %v", t.Header["alg"])
		}
		return []byte(j.secretkey), nil
	})
}
