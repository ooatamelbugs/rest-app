package entity

import "time"

type Coupon struct {
	ID        uint64    `gorm:"primary_key:auto_increment" json:"id"`
	Name      float64   `gorm:"type:varcher(255)" json:"name"`
	Amount    float64   `gorm:"type:varcher(255)" json:"amount"`
	AdminID   uint64    `gorm:"not null" json:"-"`
	Admin     Admin     `gorm:"foreignKey:AdminID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"admin"`
	ExpiresAt time.Time `gorm:"type:varcher(255)" json:"expires_at"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
