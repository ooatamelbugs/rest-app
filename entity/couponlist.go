package entity

import "time"

type CouponList struct {
	ID       uint64 `gorm:"primary_key:auto_increment" json:"id"`
	UserID   uint64 `gorm:"not null" json:"-"`
	User     User   `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"user"`
	CouponID uint64 `gorm:"not null" json:"-"`
	Coupon   Coupon `gorm:"foreignKey:CouponID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"coupon"`
	OrderID  uint64 `gorm:"not null" json:"-"`
	Order    Order  `gorm:"foreignKey:OrderID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"order"`
	CreateAt time.Time
	UpdateAt time.Time
}
