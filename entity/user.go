package entity

import "time"

// put schema of table User in db
type User struct {
	ID           uint64          `gorm:"primary_key:auto_increment" json:"id"`
	FirstName    string          `gorm:"type: varchar(255)" json:"firstname"`
	LastName     string          `gorm:"type: varchar(255)" json:"lastname"`
	Password     string          `gorm:"->;<-;not null" json:"-"`
	UserName     string          `gorm:"type: varchar(255)" json:"username"`
	Token        string          `gorm:"type:text" json:"token"`
	Cart         *Cart           `json:"cart,omitempty"`
	OrderList    *[]Order        `json:"order,omitempty"`
	FavoriteList *[]FavoriteList `json:"favoritelist,omitempty"`
	CouponList   *[]CouponList   `json:"couponlist,omitempty"`
	CreateAt     time.Time
	UpdateAt     time.Time
}
