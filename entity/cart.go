package entity

import "time"

type Cart struct {
	ID          uint64  `gorm:"primary_key:auto_increment" json:"id"`
	TotalAmount float64 `gorm:"type:decimal" json:"amount"`
	UserID      uint64  `gorm:"not null" json:"-"`
	User        User    `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"user"`
	Quantity    uint64  `gorm:"type:int" json:"quantity"`
	Status      bool    `gorm:"type:bool" json:"status"`
	Details     *[]CartDetails
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
