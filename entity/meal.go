package entity

import (
	"database/sql/driver"
	"time"
)

type MealStatus string

const (
	new  MealStatus = "NEW"
	show MealStatus = "SHOW"
)

func (ms *MealStatus) Scan(value interface{}) error {
	*ms = MealStatus(value.([]byte))
	return nil
}

func (ms MealStatus) Value() (driver.Value, error) {
	return string(ms), nil
}

// put schema of table Meal in db
type Meal struct {
	ID          uint64     `gorm:"primary_key:auto_increment" json:"id"`
	Name        string     `gorm:"type: varchar(255)" json:"name"`
	CategoryID  uint64     `gorm:"not null" json:"-"`
	Category    Category   `gorm:"foreignKey:CategoryID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"category"`
	CustomerID  uint64     `gorm:"not null" json:"-"`
	Customer    Customer   `gorm:"foreignKey:CustomerID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"customer"`
	Description string     `gorm:"type:text" json:"description"`
	Contains    string     `gorm:"type:text" json:"contains"`
	Image       string     `gorm:"type:text" json:"image"`
	Price       float64    `gorm:"type:decimal" json:"price"`
	Activate    bool       `gorm:"type:bool" json:"activate"`
	Status      MealStatus `gorm:"type:meal_status;default:'NEW'" json:"status"`
	CreateAt    time.Time
	UpdateAt    time.Time
}
