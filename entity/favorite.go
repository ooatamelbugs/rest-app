package entity

import "time"

type FavoriteList struct {
	ID        uint64 `gorm:"primary_key:auto_increment" json:"id"`
	MealID    uint64 `gorm:"not null" json:"-"`
	Meal      Meal   `gorm:"foreignKey:MealID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"meal"`
	UserID    uint64 `gorm:"not null" json:"-"`
	User      User   `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"user"`
	Status    bool   `gorm:"type:bool" json:"status"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
