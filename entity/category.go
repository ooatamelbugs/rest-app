package entity

import "time"

// put schema of table Category in db
type Category struct {
	ID          uint64   `gorm:"primary_key:auto_increment" json:"id"`
	Name        string   `gorm:"type: varchar(255)" json:"name"`
	Image       string   `gorm:"type:text" json:"image"`
	CustomerID  uint64   `gorm:"not null" json:"-"`
	Customer    Customer `gorm:"foreignKey:CustomerID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"customer"`
	Description string   `gorm:"type:text" json:"description"`
	CreateAt    time.Time
	UpdateAt    time.Time
}
