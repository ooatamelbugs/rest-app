package entity

import (
	"database/sql/driver"
	"time"
)

type StatusPaid string

const (
	paid    StatusPaid = "paid"
	pending StatusPaid = "pending"
)

func (sp *StatusPaid) Scan(value interface{}) error {
	*sp = StatusPaid(value.([]byte))
	return nil
}

func (sp StatusPaid) Value() (driver.Value, error) {
	return string(sp), nil
}

// put schema of table Order in db
type Order struct {
	ID         uint64     `gorm:"primary_key:auto_increment" json:"id"`
	Amount     float64    `gorm:"type:decimal" json:"amount"`
	Discount   float64    `gorm:"type:decimal" json:"discount"`
	UserID     uint64     `gorm:"not null" json:"-"`
	Status     StatusPaid `gorm:"type:status_paid;default:'pending'" json:"status"`
	Method     string     `gorm:"-" json:"method"`
	User       User       `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"user"`
	CustomerID uint64     `gorm:"not null" json:"-"`
	Customer   Customer   `gorm:"foreignKey:CustomerID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"customer"`
	CreateAt   time.Time
	UpdateAt   time.Time
}
