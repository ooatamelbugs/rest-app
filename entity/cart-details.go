package entity

import "time"

type CartDetails struct {
	ID        uint64  `gorm:"primary_key:auto_increment" json:"id"`
	Amount    float64 `gorm:"type:decimal" json:"amount"`
	Quantity  uint64  `gorm:"type:int" json:"quantity"`
	Status    bool    `gorm:"type:bool" json:"status"`
	MealID    uint64  `gorm:"not null" json:"-"`
	Meal      Meal    `gorm:"foreignKey:MealID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"meal"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
