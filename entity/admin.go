package entity

import "time"

type Admin struct {
	ID        uint64 `gorm:"primary_key:auto_increment" json:"id"`
	FirstName string `gorm:"type: varchar(255)" json:"firstname"`
	LastName  string `gorm:"type: varchar(255)" json:"lastname"`
	Password  string `gorm:"->;<-;not null" json:"password"`
	UserName  string `gorm:"type: varchar(255)" json:"username"`
	Role      string `gorm:"type:varchar(255);default:'Admin'" json:"role"`
	Token     string `gorm:"type:text" json:"token"`
	CreateAt  time.Time
	UpdateAt  time.Time
}
