package entity

import "time"

// put schema of table OrderDetails in db
type OrderDetails struct {
	ID       uint64 `gorm:"primary_key:auto_increment" json:"id"`
	Quantity uint64 `gorm:"type:int" json:"quantity"`
	Notes    string `gorm:"type text" json:"notes"`
	OrderID  uint64 `gorm:"not null" json:"-"`
	Order    Order  `gorm:"foreignKey:OrderID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"order"`
	MealID   uint64 `gorm:"not null" json:"-"`
	Meal     Meal   `gorm:"foreignKey:MealID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"meal"`
	CreateAt time.Time
	UpdateAt time.Time
}
