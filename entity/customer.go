package entity

import (
	"database/sql/driver"
	"time"
)

type CustomerRole string

const (
	admin CustomerRole = "Admin"
	user  CustomerRole = "user"
)

func (cr *CustomerRole) Scan(value interface{}) error {
	*cr = CustomerRole(value.([]byte))
	return nil
}

func (cr CustomerRole) Value() (driver.Value, error) {
	return string(cr), nil
}

// put schema of table User in db
type Customer struct {
	ID       uint64       `gorm:"primary_key:auto_increment" json:"id"`
	Name     string       `gorm:"type: varchar(255)" json:"name"`
	Address  string       `gorm:"type: varchar(255)" json:"address"`
	Password string       `gorm:"->;<-;not null" json:"password"`
	UserName string       `gorm:"type: varchar(255)" json:"username"`
	Role     CustomerRole `gorm:"type:customer_role" json:"role"`
	AdminID  uint64       `gorm:"not null" json:"-"`
	Token    string       `gorm:"type:text" json:"token"`
	Admin    Admin        `gorm:"foreignKey:AdminID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE" json:"admin"`
	CreateAt time.Time
	UpdateAt time.Time
}
