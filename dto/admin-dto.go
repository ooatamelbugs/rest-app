package dto

type AdminCreateDTO struct {
	FirstName string `json:"firstname" form:"firstname" binding:"required" validate:"min:3"`
	LastName  string `json:"lastname" form:"lastname" binding:"required" validate:"min:3"`
	Password  string `json:"password,omitempty" form:"password,omitempty" binding:"required" validate:"min:8"`
	UserName  string `json:"username" form:"username" binding:"required" validate:"email"`
	Image     string `json:"image" form:"image"`
	Role      string `json:"role" form:"role" binding:"required" validate:"min:4"`
}

type AdminUpdateDTO struct {
	ID        uint64 `json:"id" form:"id" binding:"required"`
	FirstName string `json:"firstname" form:"firstname" binding:"required" validate:"min:3"`
	LastName  string `json:"lastname" form:"lastname" binding:"required" validate:"min:3"`
	Password  string `json:"password,omitempty" form:"password,omitempty" binding:"required"`
	UserName  string `json:"username" form:"username" binding:"required" validate:"email"`
	Image     string `json:"image" form:"image"`
	Role      string `json:"role" form:"role" binding:"required" validate:"min:4"`
}

type AdminLoginDTO struct {
	Password string `json:"password,omitempty" form:"password,omitempty" binding:"required"`
	UserName string `json:"username" form:"username" binding:"required" validate:"email"`
}
