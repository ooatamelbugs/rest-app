package dto

type MealCreateDTO struct {
	Name        string  `json:"name" form:"name" binding:"required" validate:"min:3"`
	CategoryId  uint64  `json:"category_id,omitempty" form:"category_id,omitempty"`
	CustomerID  uint64  `json:"customer_id,omitempty" form:"customer_id,omitempty"`
	Description string  `json:"description" form:"description"`
	Contains    string  `json:"contains" form:"contains"`
	Image       string  `json:"image" form:"image"`
	Price       float64 `json:"price" form:"price"`
	Status      bool    `json:"status" form:"status"`
}

type MealUpdateDTO struct {
	ID          uint64  `json:"id" form:"id" binding:"required"`
	Name        string  `json:"name" form:"name" binding:"required" validate:"min:3"`
	CategoryId  uint64  `json:"category_id,omitempty" form:"category_id,omitempty"`
	CustomerID  uint64  `json:"customer_id,omitempty" form:"customer_id,omitempty"`
	Description string  `json:"description" form:"description"`
	Contains    string  `json:"contains" form:"contains"`
	Image       string  `json:"image" form:"image"`
	Price       float64 `json:"price" form:"price"`
	Status      bool    `json:"status" form:"status"`
}

type MealQuery struct {
	CategoryId uint64  `json:"category_id"`
	CustomerID uint64  `json:"customer_id"`
	PriceFrom  float64 `json:"price_from"`
	PriceTo    float64 `json:"price_to"`
	Status     bool    `json:"status"`
	Name       string  `json:"name"`
	Page       uint64  `json:"page"`
}
