package dto

type CustomerCreateDTO struct {
	FirstName string `json:"firstname" form:"firstname" binding:"required" validate:"min:3"`
	LastName  string `json:"lastname" form:"lastname" binding:"required" validate:"min:3"`
	Password  string `json:"password,omitempty" form:"password,omitempty" binding:"required" validate:"min:8"`
	UserName  string `json:"username" form:"username" binding:"required" validate:"email"`
	Image     string `json:"image" form:"image"`
	Role      string `json:"role" form:"role" binding:"required" validate:"min:4"`
	Address   string `json:"address" form:"address" binding:"required"`
	AdminID   uint64 `json:"admin_id,omitempty" form:"admin_id,omitempty"`
}

type CustomerUpdateDTO struct {
	ID        uint64 `json:"id" form:"id" binding:"required"`
	FirstName string `json:"firstname" form:"firstname" binding:"required" validate:"min:3"`
	LastName  string `json:"lastname" form:"lastname" binding:"required" validate:"min:3"`
	Image     string `json:"image" form:"image"`
	Password  string `json:"password,omitempty" form:"password,omitempty" binding:"required"`
	UserName  string `json:"username" form:"username" binding:"required" validate:"email"`
	Role      string `json:"role" form:"role" binding:"required"`
	Address   string `json:"address" form:"address" binding:"required"`
	AdminID   uint64 `json:"admin_id,omitempty" form:"admin_id,omitempty"`
}

type CustomerLoginDTO struct {
	Password string `json:"password,omitempty" form:"password,omitempty" binding:"required"`
	UserName string `json:"username" form:"username" binding:"required" validate:"email"`
}
