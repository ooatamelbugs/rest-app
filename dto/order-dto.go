package dto

type OrderCreateDTO struct {
	Amount     float64 `json:"amount" form:"amount" binding:"required" validate:"min:1"`
	Discount   float64 `json:"discount" form:"discount" binding:"required" validate:"min:0"`
	UserID     uint64  `json:"user_id,omitempty" form:"user_id,omitempty" binding:"required"`
	CategoryId uint64  `json:"category_id,omitempty" form:"category_id,omitempty" binding:"required"`
	Status     string  `json:"status" form:"status" binding:"required" validate:"min:0"`
	Method     string  `json:"method" form:"method"`
}

type OrderUpdateDTO struct {
	ID         uint64  `json:"id" form:"id" binding:"required"`
	Amount     float64 `json:"amount" form:"amount" binding:"required" validate:"min:1"`
	Discount   float64 `json:"discount" form:"discount" binding:"required" validate:"min:0"`
	UserID     uint64  `json:"user_id,omitempty" form:"user_id,omitempty" binding:"required"`
	CategoryId uint64  `json:"category_id,omitempty" form:"category_id,omitempty" binding:"required"`
	Status     string  `json:"status" form:"status" binding:"required" validate:"min:0"`
	Method     string  `json:"method" form:"method"`
}

type OrderDetailsCreateDTO struct {
	MealId   uint64 `json:"meal_id,omitempty" form:"meal_id,omitempty" binding:"required"`
	OrderId  uint64 `json:"order_id,omitempty" form:"order_id,omitempty" binding:"required"`
	Quantity uint64 `json:"quantity" form:"quantity" binding:"required"`
	Notes    string `json:"notes" form:"notes"`
}

type OrderDetailsUpdateDTO struct {
	ID       uint64 `json:"id" form:"id" binding:"required"`
	MealId   uint64 `json:"meal_id,omitempty" form:"meal_id,omitempty" binding:"required"`
	OrderId  uint64 `json:"order_id,omitempty" form:"order_id,omitempty" binding:"required"`
	Quantity uint64 `json:"quantity" form:"quantity" binding:"required"`
	Notes    string `json:"notes" form:"notes"`
}
