package dto

type CategoryCreateDTO struct {
	Name        string `json:"name" form:"name" binding:"required" validate:"min:3"`
	CategoryId  uint64 `json:"category_id,omitempty" form:"category_id,omitempty"`
	CustomerID  uint64 `json:"customer_id,omitempty" form:"customer_id,omitempty"`
	Description string `json:"description" form:"description"`
	Contains    string `json:"contains" form:"contains"`
	Image       string `json:"image" form:"image"`
}

type CategoryUpdateDTO struct {
	ID          uint64 `json:"id" form:"id" binding:"required"`
	Name        string `json:"name" form:"name" binding:"required" validate:"min:3"`
	CategoryId  uint64 `json:"category_id,omitempty" form:"category_id,omitempty"`
	CustomerID  uint64 `json:"customer_id,omitempty" form:"customer_id,omitempty"`
	Description string `json:"description" form:"description"`
	Contains    string `json:"contains" form:"contains"`
	Image       string `json:"image" form:"image"`
}
