package repository

import (
	"log"
	"rest-app/entity"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	InsertUser(user entity.User) entity.User
	UpdateUser(user entity.User) entity.User
	FindByUsername(UserName string) entity.User
	ProfileUser(UserID string) entity.User
	UpdateUserToken(userID uint64, token string) entity.User
	IsDuplicateUsername(UserName string) (tx *gorm.DB)
	VerifyCredetial(UserName string, Password string) interface{}
}

type userConnection struct {
	connection *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userConnection{
		connection: db,
	}
}

func (db *userConnection) InsertUser(user entity.User) entity.User {
	Password := hashAndSalt([]byte(user.Password))
	user.Password = Password
	db.connection.Save(&user)
	return user
}

func (db *userConnection) UpdateUserToken(userID uint64, token string) entity.User {
	var userTemp entity.User
	db.connection.Where("id = ?", userID).Take(&userTemp)
	userTemp.Token = token
	db.connection.Save(&userTemp)
	return userTemp
}

func (db *userConnection) UpdateUser(user entity.User) entity.User {
	if user.Password != "" {
		user.Password = hashAndSalt([]byte(user.Password))
	} else {
		var tempuser entity.User
		db.connection.Find(&tempuser, user.ID)
		user.Password = tempuser.Password
	}
	db.connection.Save(&user)
	return user
}

func (db *userConnection) IsDuplicateUsername(userName string) (tx *gorm.DB) {
	var user entity.User
	return db.connection.Where("user_name = ?", userName).Take(&user)
}

func (db *userConnection) FindByUsername(userName string) entity.User {
	var user entity.User
	db.connection.Where("user_name = ?", userName).Take(&user)
	return user
}

func (db *userConnection) ProfileUser(userID string) entity.User {
	var user entity.User
	db.connection.Find(&user, userID)
	return user
}

func (db *userConnection) VerifyCredetial(UserName string, Password string) interface{} {
	var user entity.User
	res := db.connection.Where("user_name = ?", UserName).Take(&user)
	if res.Error == nil {
		return user
	}
	return nil
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
		panic("faild to hash password")
	}
	return string(hash)
}
