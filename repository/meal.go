package repository

import (
	"encoding/json"
	"rest-app/dto"
	"rest-app/entity"

	"gorm.io/gorm"
)

type MealRepository interface {
	CreateMeal(meal entity.Meal) entity.Meal
	UpdateMeal(meal entity.Meal) entity.Meal
	DeleteMeal(meal entity.Meal) bool
	ChangeStatusMeal(mealID uint64) entity.Meal
	ShowAllMeal(query dto.MealQuery) []entity.Meal
	ShowMealByID(mealID uint64) entity.Meal
}

type mealRepository struct {
	connection *gorm.DB
}

func NewMealRepository(db *gorm.DB) MealRepository {
	return &mealRepository{
		connection: db,
	}
}

func (db *mealRepository) CreateMeal(meal entity.Meal) entity.Meal {
	db.connection.Save(&meal)
	return meal
}

func (db *mealRepository) UpdateMeal(meal entity.Meal) entity.Meal {
	db.connection.Save(&meal)
	return meal
}
func (db *mealRepository) DeleteMeal(meal entity.Meal) bool {
	db.connection.Delete(&meal)
	return true
}
func (db *mealRepository) ChangeStatusMeal(mealID uint64) entity.Meal {
	var meal entity.Meal
	db.connection.Where("id = ?", mealID).Take(&meal)
	return meal
}

func (db *mealRepository) ShowAllMeal(query dto.MealQuery) []entity.Meal {
	var ListMeal []entity.Meal
	var queryString = &query
	var inInterface map[string]interface{}
	rec, _ := json.Marshal(queryString)
	json.Unmarshal(rec, &inInterface)
	db.connection.Where(inInterface).Take(&ListMeal)
	return ListMeal
}

func (db *mealRepository) ShowMealByID(mealID uint64) entity.Meal {
	var meal entity.Meal
	db.connection.Where("id = ?", mealID).Take(&meal)
	return meal
}
