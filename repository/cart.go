package repository

import (
	"rest-app/entity"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type CartRepository interface {
	CreateCart(cart entity.Cart) entity.Cart
	AddToCart(cartItem entity.CartDetails) entity.CartDetails
	RemoveCart(userID uint64) bool
	RemoveFromCartItem(cartDetails entity.CartDetails) bool
	ShowCart(userID uint64) entity.Cart
}

type cartRepository struct {
	connection *gorm.DB
}

func NewCartRepository(db *gorm.DB) CartRepository {
	return &cartRepository{
		connection: db,
	}
}

func (db *cartRepository) CreateCart(cart entity.Cart) entity.Cart {
	db.connection.Save(&cart)
	db.connection.Preload("Details.Meal").Preload(clause.Associations).Find(&cart)
	return cart
}

func (db *cartRepository) AddToCart(cartItem entity.CartDetails) entity.CartDetails {
	db.connection.Save(&cartItem)
	db.connection.Preload("Meal").Preload(clause.Associations).Find(&cartItem)
	return cartItem
}

func (db *cartRepository) RemoveCart(userID uint64) bool {
	var cart entity.Cart
	db.connection.Where("user_id = ?", userID).Delete(&cart)
	return true
}

func (db *cartRepository) ShowCart(userID uint64) entity.Cart {
	var cart entity.Cart
	db.connection.Where("user_id = ?", userID).Preload("Details.Meal").Take(&cart)
	return cart
}

func (db *cartRepository) RemoveFromCartItem(cartDetails entity.CartDetails) bool {
	db.connection.Delete(&cartDetails)
	return true
}
