package repository

import (
	"rest-app/entity"

	"gorm.io/gorm"
)

type FavoriteListRepository interface {
	AddToFavoriteList(favoriteList entity.FavoriteList) bool
	RemoveFromFavoriteList(favoriteList entity.FavoriteList) bool
	ShowFavoriteList(userID uint64) []entity.FavoriteList
}

type favoriteListConnection struct {
	connection *gorm.DB
}

func NewFavoriteListRepository(db *gorm.DB) FavoriteListRepository {
	return &favoriteListConnection{
		connection: db,
	}
}

func (db *favoriteListConnection) AddToFavoriteList(favorite entity.FavoriteList) bool {
	db.connection.Save(&favorite)
	return true
}

func (db *favoriteListConnection) RemoveFromFavoriteList(favoriteList entity.FavoriteList) bool {
	db.connection.Delete(&favoriteList)
	return true
}

func (db *favoriteListConnection) ShowFavoriteList(userID uint64) []entity.FavoriteList {
	var favoriteList []entity.FavoriteList
	db.connection.Where("user_id = ?", userID).Preload("Meal").Take(&favoriteList)
	return favoriteList
}
