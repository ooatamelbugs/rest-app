package main

import (
	"rest-app/config"
	"rest-app/controller"
	"rest-app/repository"
	"rest-app/service"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db                 *gorm.DB                      = config.ConnectionDB()
	userRepository     repository.UserRepository     = repository.NewUserRepository(db)
	cartRepository     repository.CartRepository     = repository.NewCartRepository(db)
	userJwtService     service.JWTService            = service.NewJWTService("user")
	userAuthService    service.AuthUserService       = service.NewAuthUserService(userRepository)
	authUserController controller.AuthUserController = controller.NewAuthUserController(userAuthService, userJwtService)
)

func main() {
	defer config.CloseConnectionDB(db)
	server := gin.Default()
	authUserRoutes := server.Group("api/user")
	{
		authUserRoutes.POST("/login", authUserController.Login)
		authUserRoutes.POST("/regester", authUserController.Regester)
	}
	server.Run()
}
